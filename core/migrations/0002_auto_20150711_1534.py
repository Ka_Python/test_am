# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from core.function import clean_table, filling_table


def filling_db(apps, schema_editor):
    clean_table()
    filling_table()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(filling_db)
    ]
