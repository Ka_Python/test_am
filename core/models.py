from django.db import models


class Data(models.Model):
    name_data = models.CharField(max_length=10)
    number_data = models.IntegerField()

    class Meta:
        db_table = 'data'
