# -*- coding: utf-8 -*-

import random
from celery.task import periodic_task
from datetime import timedelta
from core.models import Data

@periodic_task(run_every=timedelta(seconds=20))
def add_to_table():
    Data.objects.create(name_data='PINIUAR', number_data=random.randrange(10, 100, 1))
    #print ('worked')

@periodic_task(run_every=timedelta(seconds=65))
def clean_table():
    Data.objects.filter(name_data='PINIUAR').delete()

