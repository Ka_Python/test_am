# -*- coding: utf-8 -*-

from django.shortcuts import render
from core.models import Data
from django.db.models import Avg


def query_test(request):
    #Getting
    try:
        number_get = Data.objects.get(number_data=573)
    except Data.DoesNotExist:
        number_get = 'EPIC_FAIL'
    except Data.MultipleObjectsReturned:
        number_get = 'FAIL'

    #Create object
    new_data = Data()
    new_data.name_data = 'PINIUGA'
    new_data.number_data = 571
    new_data.save()
    #or
    old_data = Data.objects.create(name_data='PINIAMIN', number_data=324)

    #Filtration
    more_than = Data.objects.filter(number_data__gt=300)
    range_data = Data.objects.filter(number_data__range=(350, 523))

    #Aggregation
    more_than_count = more_than.count()
    average_value = Data.objects.aggregate(average_number=Avg('number_data'))
    count = Data.objects.count()
    pass
