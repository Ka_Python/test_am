# -*- coding: utf-8 -*-

import random
import string
from core.models import Data


def clean_table():
    Data.objects.all().delete()


def filling_table():
    for i in range(1000):
        new_data = Data()
        new_data.name_data = ''. join(random.choice(string.ascii_letters)for x in range(10))
        new_data.number_data = random.randrange(100, 1000, 1)
        new_data.save()



